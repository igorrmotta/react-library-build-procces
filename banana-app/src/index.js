import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import {BananaComponent} from 'new-banana-component-build';

ReactDOM.render(
  <BananaComponent/>, document.getElementById('root'));
