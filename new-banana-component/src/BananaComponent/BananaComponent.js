import React from 'react';

class BananaComponent extends React.Component {
  render() {
    return (
      <div
        style={{
        width: '200px',
        height: '200px',
        backgroundColor: 'blue'
      }}></div>
    )
  }
}

export default BananaComponent;
