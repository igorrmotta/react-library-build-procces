'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _BananaComponent = require('./BananaComponent');

var _BananaComponent2 = _interopRequireDefault(_BananaComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _BananaComponent2.default;